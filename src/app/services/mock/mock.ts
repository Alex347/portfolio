export interface ApiData {
  id: number;
  name: string;
  description: string;
  technologies: string;
  icon: string;
  photos: string[];
  site?: string;
  gitlab: string;
}

export const dataApi: ApiData[] = [
  {
    id: 0,
    name: 'Zendic',
    description:
      // tslint:disable-next-line: max-line-length
      'Application multi plateforme adaptable mobile et navigateur web servant à faciliter la communication et l\'information entre syndicats et résidents',
    technologies: 'Symfony 4.4 et Ionic 5',
    icon: '/assets/zendic.png',
    gitlab: 'https://gitlab.com/Alex347/zendic',
    photos: [
      'assets/zendic/alerte.png',
      'assets/zendic/alerte  règlée.png',
      'assets/zendic/annonce.png',
      'assets/zendic/login.png',
      'assets/zendic/résidence.png',
      'assets/zendic/suppression annonce.png'
    ],
    site: 'https://www.zendic.fr'
  },
  {
    id: 1,
    name: 'Ashera',
    description:
      'Application mobile servant à réaliser des audits sur le réseau de l\'eau pour l\'entreprise Espélia',
    technologies: 'CodeIgniter 4 et Ionic 4',
    icon: '/assets/ashera.png',
    gitlab: 'https://gitlab.com/Alex347/ashera',
    photos: undefined
  },
  {
    id: 2,
    name: 'Piz\'aioli',
    description:
      'Site web pour la commande de pizzas et une partie admin pour le pizzaioli',
    technologies: 'Symfony 4.3 et React 16',
    icon: '/assets/pizaioli.png',
    gitlab: 'https://gitlab.com/Alex347/pizzaioli',
    photos: [
      'assets/pizza/accueil.png',
      'assets/pizza/accueil administrateur.png',
      'assets/pizza/administrateur.png',
      'assets/pizza/carte pizzas.png'
    ],
    site: 'https://pizza-front.ledainalexis.fr/'
  }
];
