import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private modals: any[] = [];
  constructor() { }
  add(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  remove(data: string) {
    // remove modal from array of active modals
    this.modals = this.modals.filter(x => x.id !== data);
  }

  open(data: string) {
    // open modal specified by id
    const modal = this.modals.find(x => x.id === data);
    modal.open();
    console.log('service', data);
  }

  close(data: string) {
    // close modal specified by id
    const modal = this.modals.find(x => x.id === data);
    modal.close();
  }
}
