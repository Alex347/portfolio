import {
  Component,
  AfterViewInit,
  ViewChild,
  OnInit
} from '@angular/core';
import { dataApi, ApiData } from './services/mock/mock';
import {
  MediaMatcher,
  BreakpointObserver,
  BreakpointState
} from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import {Title} from '@angular/platform-browser';
import {HttpClient} from '@angular/common/http';

export interface MenuItem {
  link: string;
  label: string;
  icon?: string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit, OnInit {
  @ViewChild('sideNav') public snav: MatSidenav;

  public mobileQuery: MediaQueryList;
  public mobileTinyQuery: MediaQueryList;

  public data: ApiData[] = dataApi;
  public img: any = '/assets/logo.png';

  public readonly menuItems: MenuItem[] = [
    { link: '/', label: 'Accueil', icon: 'home' },
    { link: 'project', label: 'Projets', icon: 'work' },
    { link: 'course', label: 'Mon parcours', icon: 'cached' },
    // { link: 'formation', label: 'La formation Beweb', icon: 'school' },
    { link: 'passion', label: 'Mes passions', icon: 'favorite' }
  ];

  constructor(
    public media: MediaMatcher,
    public breakpointObserver: BreakpointObserver,
    public titleService: Title
  ) {
     this.titleService.setTitle('Port Folio Le Dain Alexis');
  }

  ngOnInit(): void {
    this.mobileQuery = this.media.matchMedia('(max-width: 685px)');
    this.mobileTinyQuery = this.media.matchMedia('(max-width:599px)');
  }

  ngAfterViewInit(): void {
    this.breakpointObserver
      .observe(['(min-width: 685px)'])
      .subscribe((state: BreakpointState) => {
        this.snav.close();
      });
  }



      // .unsubscribe()





}
