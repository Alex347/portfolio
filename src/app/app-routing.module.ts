import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProjectComponent} from './pages/project/project.component';
import {CourseComponent} from './pages/course/course.component';
import {FormationComponent} from './pages/formation/formation.component';
import {HomeComponent} from './pages/home/home.component';
import {PassionComponent} from './pages/passion/passion.component';



const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  {path: '', component: HomeComponent},
  {path: 'project', component: ProjectComponent},
  {path: 'course', component: CourseComponent},
  {path: 'formation', component: FormationComponent},
  {path: 'passion', component: PassionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
