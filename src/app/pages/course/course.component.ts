import { Component, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {
  public python = '/assets/python.jfif';
  public symfony = '/assets/symfony.png';
  public angular = '/assets/angular.png';

  public mobileQuery: MediaQueryList;
  public mobileTinyQuery: MediaQueryList;
  public tabletteQuery: MediaQueryList;
  constructor(
      public media: MediaMatcher,
  ) {
  }

  ngOnInit(): void {
    this.mobileQuery = this.media.matchMedia('(max-width: 685px)');
    this.tabletteQuery = this.media.matchMedia('(max-width: 788px)');
    this.mobileTinyQuery = this.media.matchMedia('(max-width:599px)');
  }

}
