import { Component, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-passion',
  templateUrl: './passion.component.html',
  styleUrls: ['./passion.component.scss']
})
export class PassionComponent implements OnInit {
  football = 'assets/foot.jpg';
  sport = 'assets/sport.jfif';
  project = 'assets/project.jfif';
  travel = 'assets/travel.jfif';

  public mobileQuery: MediaQueryList;
  public mobileTinyQuery: MediaQueryList;
  public tabletteQuery: MediaQueryList;

  constructor(
    public media: MediaMatcher,
  ) { }

  ngOnInit(): void {
    this.mobileQuery = this.media.matchMedia('(max-width: 685px)');
    this.tabletteQuery = this.media.matchMedia('(max-width: 788px)');
    this.mobileTinyQuery = this.media.matchMedia('(max-width:599px)');
  }

}
