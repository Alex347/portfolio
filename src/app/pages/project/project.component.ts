import { Component, OnInit, ViewChild,AfterViewInit} from '@angular/core';
import {dataApi} from '../../services/mock/mock';
import {Data, Router} from '@angular/router';
import {ModalService} from '../../services/modal/modal.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ModalComponent} from '../../components/modal/modal.component';
import { MediaMatcher, BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { MatCard } from '@angular/material/card';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements  OnInit {

  public mobileQuery: MediaQueryList;
  public mobileTinyQuery: MediaQueryList;
  public tabletteQuery: MediaQueryList;
  name: string;
  color: string;
  public data = dataApi;
  selectedData: Data;

  constructor(
    public dialog: MatDialog,
    public modalComponent: ModalComponent,
    public media: MediaMatcher,
    public breakpointObserver: BreakpointObserver,
    protected router: Router
  ) { }




  ngOnInit(): void {
    this.mobileQuery = this.media.matchMedia('(max-width: 685px)');
    this.tabletteQuery = this.media.matchMedia('(max-width: 788px)');
    this.mobileTinyQuery = this.media.matchMedia('(max-width:599px)');
  }


  dataSelected(data: Data) {
    this.selectedData = data;
  }

  openModal(data: any) {
     const dialogConfig = new MatDialogConfig();
     dialogConfig.disableClose = true;
     dialogConfig.autoFocus = true;
     dialogConfig.data = data;


     this.dialog.open(ModalComponent,  dialogConfig );
  }
  openRouter(url) {
    window.location.href = url;
  }

}
