import { Component, OnInit } from '@angular/core';
import {dataApi} from '../../services/mock/mock';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public data = dataApi;

  constructor() {
  }
  ngOnInit(): void {
  }

}
