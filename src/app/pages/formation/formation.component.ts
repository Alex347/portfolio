import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formation',
  templateUrl: './formation.component.html',
  styleUrls: ['./formation.component.scss']
})
export class FormationComponent implements OnInit {
  // tslint:disable-next-line:max-line-length
  myBackgroundImageUrl = '/assets/beweb.png';
  constructor() { }

  ngOnInit(): void {
  }
}
